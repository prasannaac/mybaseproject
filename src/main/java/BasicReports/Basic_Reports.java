package BasicReports;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
//import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class Basic_Reports {

	
	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;
	
	
	@Test
	public void runReport() {
		
		html = new ExtentHtmlReporter("./report/Reports.html");
		extent = new ExtentReports();
		html.setAppendExisting(true);
		extent.attachReporter(html);
		test = extent.createTest("TC0001_Login", "Login into Leaftaps");
		test = extent.createTest("TC002", "click on leads tab");
		 
		test.assignCategory("Regression");
		test.assignAuthor("Prasanna");
		
		try {
			test.pass("Successfully logged in", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img3.png").build());
		} catch (IOException e) {
			
			e.printStackTrace();
		}

		extent.flush();
	
		
		}
	
}
