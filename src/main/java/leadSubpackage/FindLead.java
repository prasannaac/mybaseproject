package leadSubpackage;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindLead {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(05, TimeUnit.SECONDS);
		
		driver.findElementById("username").sendKeys("DemoSalesManager"); //Enter Username
		driver.findElementById("password").sendKeys("crmsfa");// Enter Password
		driver.findElementByClassName("decorativeSubmit").click(); // Click Login button
		driver.findElementByLinkText("CRM/SFA").click(); //clicking link
		
		// Find Leads
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		
		// Search by First Name
		//*[@id="ext-gen255"]
		
		WebElement fname = driver.findElementByXPath("(//input[@name='firstName'])[3]");
		fname.sendKeys("sridevi");
		//driver.findElementByName("firstName").sendKeys("Titan");
		//driver.findElementByXPath("(//button[text()='Find Leads'])[4]").click();
		driver.findElementByXPath("//button[text()=\"Find Leads\"]").click();
	}

}
