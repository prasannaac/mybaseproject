package projectActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ZoomCar {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.zoomcar.com/chennai");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		driver.findElementByLinkText("Start your wonderful journey").click();
		
		driver.findElementByXPath("//div[contains (text(), 'Nelson Manickam Road')]").click();
		Thread.sleep(3000);
		
		driver.findElementByXPath("//button[contains (text(), 'Next')]").click();
		
		Date date = new Date();
		
		DateFormat sdf = new SimpleDateFormat("dd");
		String today = sdf.format(date);
		int tmrw =Integer.parseInt(today) +2;
		System.out.println(tmrw);
			
		
		driver.findElementByXPath("//div[contains (text(), '"+tmrw+"')]").click();
		
		driver.findElementByXPath("//button[text() = 'Next']").click();
		
		driver.findElementByXPath("//button[text() = 'Done']").click();
	
		List<WebElement> results = driver.findElementsByXPath("//div[@class = 'car-listing']");
		int size = results.size();
		
		System.out.println("size = " + size);
		
		//driver.findElementByXPath("//div[contains (text(), ' Price: High to Low ')]").click();
		//System.out.println(driver.findElementByXPath("//div[@class = 'car-item']//h3").getText());
		
		List<WebElement> pricelist = driver.findElementsByXPath("//div[@class = 'price']");
		List<Integer> priceSortedList = new ArrayList<>();
		
		for (WebElement maxprice : pricelist) {
			
			String maxpri = maxprice.getText().replaceAll("[^0-9]", "");
		    int highprice = Integer.parseInt(maxpri);
		    priceSortedList.add(highprice);
		    
		}	
		
		Collections.sort(priceSortedList);
		int sortprice = priceSortedList.get(priceSortedList.size()-1);
		System.out.println(sortprice);
		
		String BrandName = driver.findElementByXPath("//div[contains (text(),'"+sortprice+"')]/preceding::h3[1]").getText();
        System.out.println(BrandName);
        
	}

}
