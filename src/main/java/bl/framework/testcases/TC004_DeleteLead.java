package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethods;
import bl.framework.api.SeleniumBase;

public class TC004_DeleteLead extends ProjectMethods {
	@Test(groups= {"Regression"})
	
	public void DuplicateLead() {
		login();
		WebElement eleCRM = locateElement("linktext", "CRM/SFA");
	  	 click(eleCRM);
		WebElement eleLeads = locateElement("linktext", "Leads");
		click(eleLeads);
		 WebElement eleFindLead = locateElement("linktext", "Find Leads");
		 click(eleFindLead);
		 
		 WebElement eleFindByFname = locateElement("xpath", "(//input[@name='firstName'])[3]");
		 clearAndType(eleFindByFname, "sridevi");
		 
		 WebElement eleFindButton = locateElement("xpath", "//button[text() = 'Find Leads']");
		 click(eleFindButton);
	
		 
	    WebElement elefirstRow = locateElement("xpath", "(//a [text() = 'Sridevi'])[1]");
	    click(elefirstRow);
	    
	    /*WebElement eleDupButton = locateElement("xpath", "//a [text() = 'Duplicate Lead']");
	    click(eleDupButton);*/
	    
	    WebElement eleDelButton = locateElement("xpath", "//a [text() = 'Delete']");
	    click(eleDelButton);
	    
	    
	    
	    
	
	}
	
	
	
}
