package bl.framework.testcases;

//import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethods;
import bl.framework.api.SeleniumBase;
//import bl.framework.api.SeleniumBase;


public class TC003_FindLead extends ProjectMethods
{
	
	@Test(groups= {"Sanity"})
	//@Test(dependsOnMethods= {"bl.framework.testcases.TC002_CreateLead.CreateLead"})
	
	public void FindLead() {
		login();
		WebElement eleCRM = locateElement("linktext", "CRM/SFA");
	   	 click(eleCRM);
		WebElement eleLeads = locateElement("linktext", "Leads");
		click(eleLeads);
		 WebElement eleFindLead = locateElement("linktext", "Find Leads");
    	 click(eleFindLead);
    	 
    	 WebElement eleFindByFname = locateElement("xpath", "(//input[@name='firstName'])[3]");
    	 clearAndType(eleFindByFname, "sridevi");
    	 
    	 WebElement eleFindButton = locateElement("xpath", "//button[text() = 'Find Leads']");
    	 click(eleFindButton);	
		
			}
	
	
	


}
