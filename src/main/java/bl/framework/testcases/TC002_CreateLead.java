package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethods;
//import bl.framework.api.SeleniumBase;
//import bl.framework.api.SeleniumBase;

public class TC002_CreateLead extends ProjectMethods{

	//@Test(groups= {"Smoke"})
	
	/*(invocationCount = 2, invocationTimeOut= 50000)*/
	
	@Test(dataProvider = "fetchData")
	public void CreateLead(String cname, String fname, String lname) {
		
		
	WebElement eleCRM = locateElement("linktext", "CRM/SFA");
   	 click(eleCRM);
   	 WebElement eleCreateLead = locateElement("linktext", "Create Lead");
   	 click(eleCreateLead);
   	 
   	 WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
   	 clearAndType(eleCompName, cname);
   	 
   	 WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
   	 clearAndType(eleFirstName, fname);
   	 
   	 WebElement elesrc_dd = locateElement("id", "createLeadForm_dataSourceId");
   	  selectDropDownUsingText(elesrc_dd, "Direct Mail");
        
   	  WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
    	 clearAndType(eleLastName, lname);
   	  
    	 WebElement eleCreateLeadButton = locateElement("xpath", "//input[@value ='Create Lead']");
    	 click(eleCreateLeadButton);
    	 driver.close();
		
	}
	@DataProvider(name ="getdata")
	public String[][] fetchdata() {
		
		String [] [] data = new String[2][3];
		data[0][0] = "Accenture";
		data[0][1] = "Sridevi";
		data[0][2] = "Kumar";
		
		data[1][0] = "Prodapt";
		data[1][1] = "Prazana";
		data[1][2] = "AC";
		
		return data;
				
		
	}
	
	
	
	
	
}
