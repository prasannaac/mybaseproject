package bi.framework.design;

//import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import bl.framework.api.SeleniumBase;
import utils.ReadExcel;


public class ProjectMethods extends SeleniumBase{

			@Parameters({"url","username","password"})
			@BeforeMethod
			public void login(String url, String username, String pswd) {
				
		    startApp("chrome", url);
			WebElement eleUsername = locateElement("id", "username");
			clearAndType(eleUsername, username); 
			WebElement elePassword = locateElement("id", "password");
			clearAndType(elePassword, pswd);
			WebElement eleLogin = locateElement("class", "decorativeSubmit");
			click(eleLogin); 
	
		
	}
			@DataProvider(name="fetchData")
			public Object[][] getData() {
				return ReadExcel.readData();
			}
}
